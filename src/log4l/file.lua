-------------------------------------------------------------------------------
-- Saves logging information in a file
--
-- @author Thiago Costa Ponte (thiago@ideais.com.br)
--
-- @copyright 2004-2013 Kepler Project
--
-------------------------------------------------------------------------------

local log4l = require('log4l')

local lastFileNameDatePattern
local lastFileHandler

local function openFileLogger(filename, datePattern)
  filename = string.format(filename, os.date(datePattern))

  if lastFileNameDatePattern ~= filename then
    local f = io.open(filename, 'a')

    if f then
      f:setvbuf('line')
      lastFileNameDatePattern = filename
      lastFileHandler = f
      return f
    else
      return nil, string.format('file %q could not be opened for writing', filename)
    end
  else
    return lastFileHandler
  end
end

function log4l.file(filename, datePattern, logPattern)
  if type(filename) ~= 'string' then
    filename = 'lualogging.log'
  end

  return log4l.new(function(self, level, message)
    local f, msg = openFileLogger(filename, datePattern)

    if not f then
      return nil, msg
    end

    local s = log4l.prepareLogMsg(logPattern, os.date(datePattern), level, message)
    f:write(s)

    return true
  end)
end

return log4l.file

