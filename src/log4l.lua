-------------------------------------------------------------------------------
-- includes a new tostring function that handles tables recursively
--
-- @author Danilo Tuler (tuler@ideais.com.br)
-- @author Andre Carregal (info@keplerproject.org)
-- @author Thiago Costa Ponte (thiago@ideais.com.br)
--
-- @copyright 2004-2013 Kepler Project
-------------------------------------------------------------------------------

local log4l = {
  -- Meta information
  _COPYRIGHT = 'Copyright (C) 2004-2013 Kepler Project',
  _DESCRIPTION = 'A simple API to use logging features in Lua',
  _VERSION = 'log4l 0.3',

  -- The DEBUG Level designates fine-grained instring.formational events that are most
  -- useful to debug an application
  DEBUG = 'DEBUG',

  -- The INFO level designates instring.formational messages that highlight the
  -- progress of the application at coarse-grained level
  INFO = 'INFO',

  -- The WARN level designates potentially harmful situations
  WARN = 'WARN',

  -- The ERROR level designates error events that might still allow the
  -- application to continue running
  ERROR = 'ERROR',

  -- The FATAL level designates very severe error events that will presumably
  -- lead the application to abort
  FATAL = 'FATAL',
}

local LEVEL = { 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL' }
local MAX_LEVELS = #LEVEL

-- make level names to order
for i = 1, MAX_LEVELS do
  LEVEL[LEVEL[i]] = i
end

-- private log function, with support for formating a complex log message.
local function LOG_MSG(self, level, fmt, ...)
  local f_type = type(fmt)
  if f_type == 'string' then
    if select('#', ...) > 0 then
      local status, msg = pcall(string.format, fmt, ...)
      if status then
        return self:append(level, msg)
      else
        return self:append(level, 'Error formatting log message: ' .. msg)
      end
    else
      -- only a single string, no formating needed.
      return self:append(level, fmt)
    end
  elseif f_type == 'function' then
    -- fmt should be a callable function which returns the message to log
    return self:append(level, fmt(...))
  end
  -- fmt is not a string and not a function, just call tostring() on it.
  return self:append(level, log4l.tostring(fmt))
end

-- create the proxy functions for each log level.
local LEVEL_FUNCS = {}
for i = 1, MAX_LEVELS do
  local level = LEVEL[i]
  LEVEL_FUNCS[i] = function(self, ...)
    -- no level checking needed here, this function will only be called if it's level is active.
    return LOG_MSG(self, level, ...)
  end
end

-- do nothing function for disabled levels.
local function disable_level() end

-- improved assertion function.
local function assert(exp, ...)
  -- if exp is true, we are finished so don't do any processing of the parameters
  if exp then return exp, ... end
  -- assertion failed, raise error
  error(string.format(...), 2)
end

-------------------------------------------------------------------------------
-- Creates a new logger object
-- @param append Function used by the logger to append a message with a
--  log-level to the log stream.
-- @return Table representing the new logger object.
-------------------------------------------------------------------------------
function log4l.new(append)
  if type(append) ~= 'function' then
    return nil, 'Appender must be a function.'
  end

  local logger = {
    append = append
  }

  function logger:setLevel(level)
    local order = LEVEL[level]
    assert(order, 'undefined level %q', tostring(level))

    if self.level then
      self:log(log4l.WARN, 'Logger: changing loglevel from %s to %s', self.level, level)
    end
    self.level = level
    self.level_order = order

    -- enable/disable levels
    for i = 1, MAX_LEVELS do
      local name = LEVEL[i]:lower()

      if i >= order then
        self[name] = LEVEL_FUNCS[i]
      else
        self[name] = disable_level
      end
    end
  end

  -- generic log function.
  function logger:log(level, ...)
    local order = LEVEL[level]
    assert(order, 'undefined level %q', tostring(level))

    if order < self.level_order then
      return
    end

    return LOG_MSG(self, level, ...)
  end

  -- initialize log level.
  logger:setLevel(log4l.DEBUG)

  return logger
end


-------------------------------------------------------------------------------
-- Prepares the log message
-------------------------------------------------------------------------------
function log4l.prepareLogMsg(pattern, dt, level, message)
  local logMsg = pattern or '%date %level %message\n'
  message = string.gsub(message, '%%', '%%%%')
  logMsg = string.gsub(logMsg, '%%date', dt)
  logMsg = string.gsub(logMsg, '%%level', level)
  logMsg = string.gsub(logMsg, '%%message', message)

  return logMsg
end


-------------------------------------------------------------------------------
-- Converts a Lua value to a string
--
-- Converts Table fields in alphabetical order
-------------------------------------------------------------------------------
local function _tostring(value, seen)
  seen = seen or {}
  local str = ''

  if type(value) ~= 'table' then
    if type(value) == 'string' then
      str = string.format('%q', value)
    else
      str = tostring(value)
    end
  else
    local mt = getmetatable(value) or {}

    if mt.__tostring then
      return tostring(value)
    end

    if seen[value] then
      return '...'
    else
      seen[value] = true
    end

    local strTable = {}
    local numTable = {}
    local outOfOrderTable = {}
    local outOfOrderKeys = {}
    local otherTable = {}
    local mapping = {}

    for k, v in ipairs(value) do
      numTable[k] = _tostring(v, seen)
    end

    for k, v in pairs(value) do
      if type(k) == 'number' then
        if k < 1 or k > #numTable then
          outOfOrderTable[k] = _tostring(v, seen)
          table.insert(outOfOrderKeys, k)
        end
      elseif type(k) == 'string' then
        table.insert(strTable, k)
      else
        local as_string = _tostring(k, seen)
        mapping[as_string] = mapping[as_string] or {}
        table.insert(otherTable, as_string)
        table.insert(mapping[as_string], k)
      end
    end

    table.sort(outOfOrderKeys)
    table.sort(strTable)
    table.sort(otherTable)

    str = str..'{'
    local separator = ''

    for _, v in ipairs(numTable) do
      str = str .. separator .. v
      separator = ', '
    end

    for _, v in ipairs(outOfOrderKeys) do
      str = str .. separator .. '[' .. v .. ']' .. ' = ' .. outOfOrderTable[v]
      separator = ', '
    end

    for _, fieldName in ipairs(strTable) do
      str = str .. separator .. fieldName .. ' = ' .. _tostring(value[fieldName], seen)
      separator = ', '
    end

    for _, fieldName in ipairs(otherTable) do
      for _, field in ipairs(mapping[fieldName]) do
        str = str .. separator .. '[' .. fieldName .. ']' .. ' = ' .. _tostring(value[field], seen)
        separator = ', '
      end
    end

    str = str .. '}'
  end

  seen[value] = nil

  return str
end

log4l.tostring = _tostring

return log4l
